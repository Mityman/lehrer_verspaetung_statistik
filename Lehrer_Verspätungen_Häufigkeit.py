from matplotlib import pyplot as plt
import numpy as np
from collections import Counter

def häufigkeiten(data):
    counter = dict()
    keys,values = [],[]
    for l in data:
        counter.update({l: counter.get(l, 0) + 1})

    counter = dict(sorted(counter.items()))
    print(counter)

    for k in counter.keys():
        keys.append(k)

    for v in counter.values():
        print(v)
        values.append(v)
        print(values)

    return keys,values

def addlabels(x,y):
    ycounter,temp = 0,0
    for i in range(max(x)+1):
        if i in x:
            plt.text(i,y[ycounter]/2,y[ycounter], ha = 'center',color='red')
            ycounter += 1


Verspätung= [14,8,8,9,8,4,7,4,0,9,9,4,3,4,8,4,6,5,17,8,9,15,4] # {14: 1, 8: 5, 9: 4, 4: 6, 7: 1, 0: 1, 3: 1, 6: 1, 5: 1, 17: 1, 15: 1}
                                                               # {0: 1, 3: 1, 4: 6, 5: 1, 6: 1, 7: 1, 8: 5, 9: 4, 14: 1, 15: 1, 17: 1}
Minuten,Häufigkeiten = häufigkeiten(Verspätung)
print(Minuten)
print(Häufigkeiten)

plt.bar(Minuten,Häufigkeiten,color = 'darkblue')
addlabels(Minuten,Häufigkeiten)
plt.title('Häufigkeit der Längen der Verspäterungen')
plt.xlabel('Verspätung in Minuten')
plt.ylabel('Häufigkeit der bestimmten Verspätung')
plt.xticks([i for i in range(20)])
plt.show()
