from matplotlib import pyplot as plt

Lehrer = ["Eder", "Strohmaier", "Wiltsche", "Ortner", "Blatnig", "Oberlercher", "Klausner", "Winkler","Karasek", "Kofler", "Egger"]
Eder = [14,8]
Strohmaier = [9,8]
Wiltsche = [8,4]
Ortner = [7,4]
Blatnig = [0,5]
Oberlecher = [9,9]
Klausner = [4,5]
Winkler = [3]
Karasek = [4,8]
Kofler = [4]
Egger = [6]
List_of_Lists = [Eder,Strohmaier,Wiltsche,Ortner,Blatnig,
                 Oberlecher,Klausner,Winkler,Karasek,Kofler,Egger]

def get_list_of_averages(list):
    averages = []
    value, avg = 0,0
    for i in list:
        averages.append(average(i))
    return averages

def average(list):
    value = 0
    for i in list:
        value += i
    return value/len(list)

def addlabels(x,y):
    for i in range(len(x)):
        plt.text(i,y[i]//2,y[i], ha = 'center', color = 'white')

#print(average(Strohmaier))

List_of_Averages = get_list_of_averages(List_of_Lists)

plt.bar(range(len(Lehrer)),List_of_Averages, color = 'darkblue')
addlabels(Lehrer,List_of_Averages)

plt.title('Durchschnittsverspätungen')

plt.xlabel('Lehrer')
plt.ylabel('Verspätung in Minuten')

plt.xticks(range(len(Lehrer)),Lehrer)
plt.show()